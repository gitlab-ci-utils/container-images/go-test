# Changelog

## v2.7.1 (2025-03-04)

### Changed

- Updated base image to `golang:1.24.1-alpine3.21`.

## v2.7.0 (2025-02-16)

### Changed

- Updated base image to `golang:1.24.0-alpine3.21`.

## v2.6.1 (2025-02-06)

### Fixed

- Updated base image to latest `golang:1.23.6-alpine3.20` security patch.

## v2.6.0 (2025-02-04)

### Changed

- Updated `uncover` to v0.0.2. See the
  [CHANGELOG](https://github.com/rsc/uncover/compare/v0.0.1...v0.0.2)
  for details.

### Fixed

- Updated base image to `golang:1.23.6-alpine3.21`.

## v2.5.8 (2025-01-18)

### Fixed

- Updated base image to latest `golang:1.23.5-alpine3.21` security patch.

## v2.5.7 (2025-01-09)

### Fixed

- Updated base image to latest `golang:1.23.4-alpine3.21` security patch.

## v2.5.6 (2024-12-11)

### Fixed

- Updated base image to `golang:1.23.4-alpine3.21`.

## v2.5.5 (2024-11-07)

### Fixed

- Updated base image to `golang:1.23.3-alpine3.20`.

## v2.5.4 (2024-10-22)

### Fixed

- Updated `gocover-cobertura` to v1.3.0.

## v2.5.3 (2024-10-02)

### Fixed

- Updated base image to `golang:1.23.2-alpine3.20`.

### Miscellaneous

- Updated CI pipeline to use template job for image `annotations`.

## v2.5.2 (2024-09-17)

### Fixed

- Fixed CI pipeline to update image `annotations` to be correct for this
  project. Previously, the `annotations` from the base `golang` image were
  cascading to this image.

## v2.5.1 (2024-09-08)

### Fixed

- Updated base image to `golang:1.23.1-alpine3.20`.

## v2.5.0 (2024-08-14)

### Changed

- Updated base image to `golang:1.23.0-alpine3.20`.

## v2.4.3 (2024-08-11)

### Fixed

- Updated base image to `golang:1.22.6-alpine3.20`.

## v2.4.2 (2024-07-03)

### Fixed

- Updated base image to `golang:1.22.5-alpine3.20`.

## v2.4.1 (2024-06-04)

### Fixed

- Updated base image to `golang:1.22.4-alpine3.20`.

## v2.4.0 (2024-05-23)

### Changed

- Updated base image to `golang:1.22.3-alpine3.20`.

### Miscellaneous

- Update renovate presets to v1.1.0.

## v2.3.2 (2024-05-07)

### Fixed

- Updated base image to `golang:1.22.3-alpine3.19`.

## v2.3.1 (2024-04-03)

### Fixed

- Updated base image to `golang:1.22.2-alpine3.19`.

## v2.3.0 (2024-03-22)

### Changed

- Added [`gocovmerge`](https://pkg.go.dev/github.com/wadey/gocovmerge) utility
  to merge `-coverprofile` text formatted coverage files. (#6)

## v2.2.1 (2024-03-05)

### Fixed

- Updated base image to `golang:1.22.1-alpine3.19`.

### Miscellaneous

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#5)

## v2.2.0 (2024-02-07)

### Changed

- Updated base image to `golang:1.22.0-alpine3.19`.

## v2.1.1 (2024-02-06)

### Fixed

- Updated base image to `golang:1.21.7-alpine3.19`.

## v2.1.0 (2024-02-02)

### Changed

- Added [`rsc.io/uncover`](https://pkg.go.dev/rsc.io/uncover) utility to create
  a report showing uncovered lines of code from a coverage report. (#4)

## v2.0.1 (2024-01-09)

### Fixed

- Updated base image to `golang:1.21.6-alpine3.18`.

## v2.0.0 (2023-12-10)

### Changed

- BREAKING: Updated base image to Alpine 3.19.

### Miscellaneous

- Updated Renovate config to track Alpine-based `golang` images in Dockerfile.
  (#3)

## v1.0.2 (2023-12-08)

### Fixed

- Updated base image to `golang:1.21.5-alpine3.18`.

## v1.0.1 (2023-11-09)

### Fixed

- Updated base image to `golang:1.21.4-alpine3.18`.

## v1.0.0 (2023-10-29)

Initial release
