FROM golang:1.24.1-alpine3.21@sha256:43c094ad24b6ac0546c62193baeb3e6e49ce14d3250845d166c77c25f64b0386

# OS packages gcc and musl-dev required for the go -race flag, see
# https://github.com/golang/go/issues/14481. Git required to
# clone new repositories.
# Note that each `go install` command must be on its own line, and on a single
# line, to be properly managed by Renovate.
# hadolint ignore=DL3018
RUN apk add --no-cache git gcc musl-dev && \
  # Required to generate a cobertura coverage report from go test -cover.
  go install github.com/boumenot/gocover-cobertura@v1.3.0 && \
  # Required to generate a junit test report from go test results.
  go install github.com/jstemmer/go-junit-report/v2@v2.1.0 && \
  # Required to generate a report showing the uncovered lines from go test -cover.
  go install rsc.io/uncover@v0.0.2 && \
  # Required to merge `-coverprofile` text formatted coverage files.
  go install github.com/wadey/gocovmerge@v0.0.0-20160331181800-b5bfa59ec0ad

LABEL org.opencontainers.image.licenses="Apache-2.0"
LABEL org.opencontainers.image.source="https://gitlab.com/gitlab-ci-utils/container-images/go-test"
LABEL org.opencontainers.image.title="go-test"
LABEL org.opencontainers.image.url="https://gitlab.com/gitlab-ci-utils/container-images/go-test"
