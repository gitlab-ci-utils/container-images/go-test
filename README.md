# Go Test

A `go:alpine` based container image to run Go tests. Includes the OS packages
required to run with the `-race` flag on Alpine (for example `gcc`), as well as Go
tools to process code coverage reports.

## Install

The image can be pulled from the GitLab Container Registry:

```sh
# docker pull registry.gitlab.com/gitlab-ci-utils/container-images/go-test:latest
```

All available container image tags are available in the
`gitlab-ci-utils/container-images/go-test` repository at
<https://gitlab.com/gitlab-ci-utils/container-images/go-test/container_registry>.
The following container image tags are available:

- `vX.Y.Z`: Tags for specific `go-test` versions (for example `v1.0.0`)
- `latest`: Tag corresponding to the latest commit to the `main` branch of this
  repository

**Note:** All other image tags in this repository, and any images in the
`gitlab-ci-utils/container-images/go-test/tmp` repository, are temporary images
used during the build process and may be deleted at any point.

## Usage

### GitLab CI

The following is an example job from a `.gitlab-ci.yml` file to use this image
to run go tests with coverage reports and with `-race`:

``` yml
go_test:
  image: registry.gitlab.com/gitlab-ci-utils/container-images/go-test:latest
  stage: test
  script:
    - go test -v -coverprofile coverage.out -covermode count ./... | tee tests.txt
    # Generate test coverage by function, with total coverage for GitLab
    - go tool cover -func coverage.out
  after_script:
    # Generate Cobertura coverage report for GitLab
    - gocover-cobertura -by-files < coverage.out > coverage.xml
    # Generate HTML coverage report
    - go tool cover -html coverage.out -o coverage.html
    # Generate JUnit report for GitLab
    - go-junit-report < tests.txt > junit.xml
    # Generate report showing uncovered lines
    - uncover coverage.out > uncovered.txt
  coverage: '/total:\s+\(statements\)\s+\d+.\d+%/'
  artifacts:
    expose_as: 'Go Test Coverage Report'
    paths:
      - coverage.html
      - uncovered.txt
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
      junit: junit.xml

go_test_race:
  image: registry.gitlab.com/gitlab-ci-utils/container-images/go-test:latest
  stage: test
  variables:
    # Cgo is required for -race.
    CGO_ENABLED: 1
  script:
    - go test ./... -race
```

The image also includes the `gocovmerge` tool, which can be used to merge
`-coverprofile` text formatted coverage files.

``` yml
go_test:
  ...
  script:
    - go test -v -coverprofile coverage.out -covermode count ./... | tee tests.txt
    # Merge coverage files, if required. This example assumes
    # coverage-int.out is generated elsewhere.
    - gocovmerge coverage.out coverage-int.out > coverage-merged.out
    # Generate test coverage by function, with total coverage for GitLab
    - go tool cover -func coverage-merged.out
  ...
```
